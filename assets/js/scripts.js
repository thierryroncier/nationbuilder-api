var websiteurl = "https://nbapi.reputationsquad.com";

////////////
// PEOPLE //
////////////

document.querySelector("#peopleadd").addEventListener( 'click', function(event) {
  document.querySelector("#formpeopleadd").style.display = '';
});

document.querySelector(".formpeopleaddbutton").addEventListener( 'click', function(event) {
  var formadd          = this.parentNode;
  var formaddfirstname = formadd.querySelector(".formaddfirstname").value;
  var formaddlastname  = formadd.querySelector(".formaddlastname").value;
  var formaddemail 		 = formadd.querySelector(".formaddemail").value;
  var formaddapicode 	 = formadd.querySelector(".apicode").value;
  var formaddapitoken  = formadd.querySelector(".apitoken").value;

  if ( formaddapicode != '' && formaddapitoken != '' && formaddfirstname != '' && formaddlastname != '' && formaddemail != '' ) {
    // On envoie dans l'API
    var data = "code="+formaddapicode+"&token="+formaddapitoken+"&formaddfirstname="+formaddfirstname+"&formaddlastname="+formaddlastname+"&formaddemail="+formaddemail;

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", websiteurl+"/functions/people-add.php", true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhttp.send(data);

    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4) {
        var response = JSON.parse(xhttp.responseText);
        document.location.reload(true);
      }
    }
  }
});

var peoplechange = document.querySelectorAll(".peoplechange");
Array.prototype.forEach.call(peoplechange, function(epeoplechange, i){
  epeoplechange.addEventListener( 'click', function(e) {
    this.parentNode.querySelector('.formpeoplechange').style.display = '';
  });
});

var peoplechangebutton = document.querySelectorAll(".formpeoplechangebutton");
Array.prototype.forEach.call(peoplechangebutton, function(epeoplechange, i){
  epeoplechange.addEventListener( 'click', function(e) {
    var formchange          = this.parentNode;
    var formchangeid 				= formchange.querySelector(".formchangeid").value;
    var formchangefirstname = formchange.querySelector(".formchangefirstname").value;
    var formchangelastname 	= formchange.querySelector(".formchangelastname").value;
    var formchangeemail 		= formchange.querySelector(".formchangeemail").value;
    var formchangeapicode 	= formchange.querySelector(".apicode").value;
    var formchangeapitoken 	= formchange.querySelector(".apitoken").value;

    if (formchangeid != '' && formchangeapicode != '' && formchangeapitoken != '' && formchangeemail != '') {
      // On envoie dans l'API
      var data = "code="+formchangeapicode+"&token="+formchangeapitoken+"&formchangeid="+formchangeid+"&formchangefirstname="+formchangefirstname+"&formchangelastname="+formchangelastname+"&formchangeemail="+formchangeemail;

      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", websiteurl+"/functions/people-change.php", true);
      xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      xhttp.send(data);

      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          var response = JSON.parse(xhttp.responseText);
          document.location.reload(true);
        }
      }
    }
  });
});

var peopledeletebutton = document.querySelectorAll(".formpeopledelete");
Array.prototype.forEach.call(peopledeletebutton, function(epeoplechange, i){
  epeoplechange.addEventListener( 'click', function(e) {
    var formchange          = this.parentNode;
    var formchangeid 				= formchange.querySelector(".formchangeid").value;
    var formchangeapicode 	= formchange.querySelector(".apicode").value;
    var formchangeapitoken 	= formchange.querySelector(".apitoken").value;

    if (formchangeid != '' && formchangeapicode != '' && formchangeapitoken != '') {
      // On envoie dans l'API
      var data = "code="+formchangeapicode+"&token="+formchangeapitoken+"&formchangeid="+formchangeid;

      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", websiteurl+"/functions/people-delete.php", true);
      xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      xhttp.send(data);

      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          var response = JSON.parse(xhttp.responseText);
          document.location.reload(true);
        }
      }
    }
  });
});



////////////
// EVENTS //
////////////

var eventchange = document.querySelectorAll(".eventchange");
Array.prototype.forEach.call(eventchange, function(eeventchange, i){
    eeventchange.addEventListener( 'click', function(e) {
    this.parentNode.querySelector('.formeventchange').style.display = '';
  });
});

var eventchange = document.querySelectorAll(".formeventchangebutton");
Array.prototype.forEach.call(eventchange, function(eeventchange, i){
    eeventchange.addEventListener( 'click', function(e) {

      var formevent = this.parentNode;

      var eventchangetitle     = formevent.querySelector(".eventchangetitle").value;
      var eventchangeintro     = formevent.querySelector(".eventchangeintro").value;
      var eventchangestartdate = formevent.querySelector(".eventchangestartdate").value;
      var eventchangestarthour = formevent.querySelector(".eventchangestarthour").value;
      var eventchangeenddate 	 = formevent.querySelector(".eventchangeenddate").value;
      var eventchangeendhour 	 = formevent.querySelector(".eventchangeendhour").value;
      var eventid 	           = formevent.querySelector(".eventid").value;

      var eventapicode 	= formevent.querySelector(".eventapicode").value;
      var eventapitoken = formevent.querySelector(".eventapitoken").value;

      if (eventid != '' && eventchangetitle != '' && eventapicode != '' && eventapitoken != '') {
        // On envoie dans l'API
          var data = "code="+eventapicode+"&token="+eventapitoken+"&eventchangetitle="+eventchangetitle+"&eventchangeintro="+eventchangeintro+"&eventchangestartdate="+eventchangestartdate+"&eventchangestarthour="+eventchangestarthour+"&eventchangeenddate="+eventchangeenddate+"&eventchangeendhour="+eventchangeendhour+"&eventid="+eventid;

          var xhttp = new XMLHttpRequest();
          xhttp.open("POST", websiteurl+"/functions/event-change.php", true);
          xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
          xhttp.send(data);

          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4) {
              var response = JSON.parse(xhttp.responseText);
              document.location.reload(true);
            }
          }
      }
  });
});


var eventdelete = document.querySelectorAll(".formeventdeletebutton");
Array.prototype.forEach.call(eventdelete, function(eeventchange, i){
  eeventchange.addEventListener( 'click', function(e) {

    var formchange          = this.parentNode;
    var eventid 				    = formchange.querySelector(".eventid").value;
    var formchangeapicode 	= formchange.querySelector(".eventapicode").value;
    var formchangeapitoken 	= formchange.querySelector(".eventapitoken").value;

    if (eventid != '' && formchangeapicode != '' && formchangeapitoken != '') {
      // On envoie dans l'API
      var data = "code="+formchangeapicode+"&token="+formchangeapitoken+"&eventid="+eventid;

      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", websiteurl+"/functions/event-delete.php", true);
      xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      xhttp.send(data);

      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          var response = JSON.parse(xhttp.responseText);
          document.location.reload(true);
        }
      }
    }
  });
});

document.querySelector("#eventadd").addEventListener( 'click', function(event) {
  document.querySelector("#formeventadd").style.display = '';
});

document.querySelector(".formeventaddbutton").addEventListener( 'click', function(event) {
  var formevent = this.parentNode;

  var eventaddtitle = formevent.querySelector(".eventaddtitle").value;
  var eventaddintro = formevent.querySelector(".eventaddintro").value;
  var eventaddstartdate = formevent.querySelector(".eventaddstartdate").value;
  var eventaddstarthour = formevent.querySelector(".eventaddstarthour").value;
  var eventaddenddate 	= formevent.querySelector(".eventaddenddate").value;
  var eventaddendhour 	= formevent.querySelector(".eventaddendhour").value;

  var eventapicode 	= formevent.querySelector(".eventapicode").value;
  var eventapitoken = formevent.querySelector(".eventapitoken").value;

  if (eventaddtitle != '' && eventapicode != '' && eventapitoken != '') {
    // On envoie dans l'API
      var data = "code="+eventapicode+"&token="+eventapitoken+"&eventaddtitle="+eventaddtitle+"&eventaddintro="+eventaddintro+"&eventaddstartdate="+eventaddstartdate+"&eventaddstarthour="+eventaddstarthour+"&eventaddendhour="+eventaddendhour;

      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", websiteurl+"/functions/event-add.php", true);
      xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      xhttp.send(data);

      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
          var response = JSON.parse(xhttp.responseText);
          document.location.reload(true);
        }
      }
  }
});
