<?php
header('Content-type: application/json');

$eventaddtitle 	= '';
$eventaddintro 	= '';
$eventaddstartdate 	= '';
$eventaddstarthour 	= '';
$eventaddenddate = '';
$eventaddendhour = '';
$code 	= '';
$token 	= '';

if (!empty($_POST)) :
	$code 							= $_POST['code'];
	$token 							= $_POST['token'];
	$eventaddtitle 			= $_POST['eventaddtitle'];
	$eventaddintro 			= $_POST['eventaddintro'];
	$eventaddstartdate 	= $_POST['eventaddstartdate'];
	$eventaddstarthour 	= $_POST['eventaddstarthour'];
	$eventaddenddate 		= $_POST['eventaddenddate'];
	$eventaddendhour 		= $_POST['eventaddendhour'];
endif;

if (!empty($eventaddtitle) && $eventaddtitle != '' && !empty($code) && $code != '' && !empty($token) && $token != '') :

	define('ROOT', getcwd());

	// OAuth 2 Library
	require_once ROOT . '/../OAuth2/Client.php';
	require_once ROOT . '/../OAuth2/GrantType/IGrantType.php';
	require_once ROOT . '/../OAuth2/GrantType/AuthorizationCode.php';


	// Client ID and Secret from Nation Builder
	define('CLIENT_ID', 'd651257f5dfc894fafd554ddf0bfc73d65aed8daad37d8e044be10d782587f5f');
	define('CLIENT_SECRET', '167f25249aacc26a5b9dfa5389bf60a9018168dbec35c9efaabb36fbf55db63e');

	// Constants we need to talk to Nation Builder
	define('WEBSITE_SLUG', "reputationsquaddev");
	define('REQUEST_ENDPOINT', "https://reputationsquaddev.nationbuilder.com/api/v1");

	// Start a new OAuth2 Client
	$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);

	$client->setAccessTokenType(1);

	// Set our token
	$client->setAccessToken($token);

	// Set the headers for the request
	$header = array(
		'Authorization' => $token,
		'Content-Type' => 'application/json',
		'Accept' => 'application/json'
		);

	if ($eventaddstartdate != '') :
		if ($eventaddstarthour != '') :
			$datestart = $eventaddstartdate . 'T' . $eventaddstarthour . ':00+01:00';
		else :
			$datestart = $eventaddstartdate . 'T' . '20:00:00';
		endif;
	else :
		// Today
		if ($eventaddstarthour != '') :
			$datestart = date('Y-m-d') . 'T' . $eventaddstarthour . ':00+01:00';
		else :
			$datestart = date('Y-m-d') . 'T' . '20:00:00';
		endif;
	endif;
	if ($eventaddenddate != '') :
		if ($eventaddendhour != '') :
			$dateend = $eventaddenddate . 'T' . $eventaddendhour . ':00+01:00';
		else :
			$dateend = $eventaddenddate . 'T' . '22:00:00';
		endif;
	else :
		// Start
		if ($eventaddendhour != '') :
			$dateend = $eventaddstartdate . 'T' . $eventaddendhour . ':00+01:00';
		else :
			$dateend = $eventaddstartdate . 'T' . '22:00:00';
		endif;
	endif;

	//Structure d'un event :
	$params = array(
	  "event"=> array(
	    "status"=> "published",
	    "name"=> $eventaddtitle,
	    "intro"=> $eventaddintro,
	    // "time_zone"=> "Paris",
	    "start_time"=> $datestart,
	    "end_time"=> $dateend,
	    "contact"=> array(
	      "name"=> "Mathieu Preau",
	      // "contact_phone"=> "0606060606",
	      // "show_phone"=> true,
	      "contact_email"=> "contact@reputationsquad.com",
	      "email"=> "contact@reputationsquad.com",
	      "show_email"=> true
	    ),
	    "rsvp_form"=> array(
	      "phone"=> "optional",
	      "address"=> "required",
	      "allow_guests"=> true,
	      "accept_rsvps"=> true,
	      "gather_volunteers"=> true
	    ),
	    "show_guests"=> true,
	    "capacity"=> 30,
	    "venue"=> array(
	      "name"=> "RS",
	      "address"=> array(
	        "address1"=> "rue Beaubourg",
	        "city"=> "Paris",
	        "state"=> "FR"
	      )
	    )
	  )
	);

	// Create/sites/:site_slug/pages/events
	$response = $client->fetch(REQUEST_ENDPOINT . '/sites/' . WEBSITE_SLUG . '/pages/events?access_token='.$token, json_encode($params), 'POST',$header);

	echo json_encode($response);
else :
	$error = "ID ou token incorrect";
	echo json_encode($error);
endif;
