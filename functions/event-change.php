<?php
header('Content-type: application/json');

$eventchangetitle 	= '';
$eventchangeintro 	= '';
$eventchangestartdate 	= '';
$eventchangestarthour 	= '';
$eventchangeenddate = '';
$eventchangeendhour = '';
$eventid = '';
$code 	= '';
$token 	= '';

if (!empty($_POST)) :
	$code 							= $_POST['code'];
	$token 							= $_POST['token'];
	$eventid 						= $_POST['eventid'];
	$eventchangetitle 			= $_POST['eventchangetitle'];
	$eventchangeintro 			= $_POST['eventchangeintro'];
	$eventchangestartdate 	= $_POST['eventchangestartdate'];
	$eventchangestarthour 	= $_POST['eventchangestarthour'];
	$eventchangeenddate 		= $_POST['eventchangeenddate'];
	$eventchangeendhour 		= $_POST['e/..ventchangeendhour'];
endif;

if (!empty($eventid) && $eventid != '' && !empty($eventchangetitle) && $eventchangetitle != '' && !empty($code) && $code != '' && !empty($token) && $token != '') :

	define('ROOT', getcwd());

	// OAuth 2 Library
	require_once ROOT . '/../OAuth2/Client.php';
	require_once ROOT . '/../OAuth2/GrantType/IGrantType.php';
	require_once ROOT . '/../OAuth2/GrantType/AuthorizationCode.php';


	// Client ID and Secret from Nation Builder
	define('CLIENT_ID', 'd651257f5dfc894fafd554ddf0bfc73d65aed8daad37d8e044be10d782587f5f');
	define('CLIENT_SECRET', '167f25249aacc26a5b9dfa5389bf60a9018168dbec35c9efaabb36fbf55db63e');

	// Constants we need to talk to Nation Builder
	define('WEBSITE_SLUG', "reputationsquaddev");
	define('REQUEST_ENDPOINT', "https://reputationsquaddev.nationbuilder.com/api/v1");

	// Start a new OAuth2 Client
	$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);

	$client->setAccessTokenType(1);

	// Set our token
	$client->setAccessToken($token);

	// Set the headers for the request
	$header = array(
		'Authorization' => $token,
		'Content-Type' => 'application/json',
		'Accept' => 'application/json'
		);

	if ($eventchangestartdate != '') :
		if ($eventchangestarthour != '') :
			$datestart = $eventchangestartdate . 'T' . $eventchangestarthour . ':00+01:00';
		else :
			$datestart = $eventchangestartdate . 'T' . '20:00:00';
		endif;
	else :
		// Today
		if ($eventchangestarthour != '') :
			$datestart = date('Y-m-d') . 'T' . $eventchangestarthour . ':00+01:00';
		else :
			$datestart = date('Y-m-d') . 'T' . '20:00:00';
		endif;
	endif;
	if ($eventchangeenddate != '') :
		if ($eventchangeendhour != '') :
			$dateend = $eventchangeenddate . 'T' . $eventchangeendhour . ':00+01:00';
		else :
			$dateend = $eventchangeenddate . 'T' . '22:00:00';
		endif;
	else :
		// Start
		if ($eventchangeendhour != '') :
			$dateend = $eventchangestartdate . 'T' . $eventchangeendhour . ':00+01:00';
		else :
			$dateend = $eventchangestartdate . 'T' . '22:00:00';
		endif;
	endif;

	//Structure d'un event :
	$params = array(
	  "event"=> array(
	    "status"=> "published",
	    "name"=> $eventchangetitle,
	    "intro"=> $eventchangeintro,
	    // "time_zone"=> "Paris",
	    "start_time"=> $datestart,
	    "end_time"=> $dateend,
	    "contact"=> array(
	      "name"=> "Mathieu Preau",
	      // "contact_phone"=> "0606060606",
	      // "show_phone"=> true,
	      "contact_email"=> "contact@reputationsquad.com",
	      "email"=> "contact@reputationsquad.com",
	      "show_email"=> true
	    ),
	    "rsvp_form"=> array(
	      "phone"=> "optional",
	      "address"=> "required",
	      "allow_guests"=> true,
	      "accept_rsvps"=> true,
	      "gather_volunteers"=> true
	    ),
	    "show_guests"=> true,
	    "capacity"=> 30,
	    "venue"=> array(
	      "name"=> "RS",
	      "address"=> array(
	        "address1"=> "rue Beaubourg",
	        "city"=> "Paris",
	        "state"=> "FR"
	      )
	    )
	  )
	);

	$response = $client->fetch(REQUEST_ENDPOINT . '/sites/' . WEBSITE_SLUG . '/pages/events/'.$eventid.'?access_token='.$token, json_encode($params), 'PUT',$header);

	echo json_encode($response);
else :
	$error = "ID ou token incorrect : ".$code.' '.$token.' '.$eventid.' '.$eventchangetitle;
	echo json_encode($error);
endif;
