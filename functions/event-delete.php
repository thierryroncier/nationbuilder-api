<?php
header('Content-type: application/json');

$eventid = '';
$code = '';
$token = '';

if (!empty($_POST)) :
	$code 				= $_POST['code'];
	$token 				= $_POST['token'];
	$eventid 			= $_POST['eventid'];
endif;

if (!empty($eventid) && $eventid != '' && !empty($code) && $code != '' && !empty($token) && $token != '') :

	define('ROOT', getcwd());

	// OAuth 2 Library
	require_once ROOT . '/../OAuth2/Client.php';
	require_once ROOT . '/../OAuth2/GrantType/IGrantType.php';
	require_once ROOT . '/../OAuth2/GrantType/AuthorizationCode.php';


	// Client ID and Secret from Nation Builder
	define('CLIENT_ID', 'd651257f5dfc894fafd554ddf0bfc73d65aed8daad37d8e044be10d782587f5f');
	define('CLIENT_SECRET', '167f25249aacc26a5b9dfa5389bf60a9018168dbec35c9efaabb36fbf55db63e');

	// Constants we need to talk to Nation Builder
	define('WEBSITE_SLUG', "reputationsquaddev");
	define('REQUEST_ENDPOINT', "https://reputationsquaddev.nationbuilder.com/api/v1");

	// Start a new OAuth2 Client
	$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);

	$client->setAccessTokenType(1);

	// Set our token
	$client->setAccessToken($token);

	// Set the headers for the request
	$header = array(
		'Authorization' => $token,
		'Content-Type' => 'application/json',
		'Accept' => 'application/json'
		);

	$params = array(
	  // 'person' => array(
			'id' => $eventid
		// )
	);

	$response = $client->fetch(REQUEST_ENDPOINT . '/sites/'.WEBSITE_SLUG.'/pages/events/'.$eventid.'?access_token='.$token, json_encode($params), 'DELETE',$header);

	echo json_encode($response);
else :
	echo "ID ou token incorrect";
endif;
