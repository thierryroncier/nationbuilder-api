<?php
header('Content-type: application/json');

$code = '';
$token = '';
$formchangefirstname = '';
$formchangelastname = '';
$formchangeemail = '';

if (!empty($_POST)) :
	$code = $_POST['code'];
	$token = $_POST['token'];
	$formchangefirstname = $_POST['formchangefirstname'];
	$formchangelastname = $_POST['formchangelastname'];
	$formchangeemail = $_POST['formchangeemail'];
endif;

if (!empty($code) && $code != '' && !empty($token) && $token != '' && !empty($formchangefirstname) && $formchangefirstname != '' && !empty($formchangelastname) && $formchangelastname != '' && !empty($formchangeemail) && $formchangeemail != '') :

	define('ROOT', getcwd());

	// OAuth 2 Library
	require_once ROOT . '/../OAuth2/Client.php';
	require_once ROOT . '/../OAuth2/GrantType/IGrantType.php';
	require_once ROOT . '/../OAuth2/GrantType/AuthorizationCode.php';

	// Client ID and Secret from Nation Builder
	define('CLIENT_ID', 'd651257f5dfc894fafd554ddf0bfc73d65aed8daad37d8e044be10d782587f5f');
	define('CLIENT_SECRET', '167f25249aacc26a5b9dfa5389bf60a9018168dbec35c9efaabb36fbf55db63e');

	define('REQUEST_ENDPOINT', "https://reputationsquaddev.nationbuilder.com/api/v1");

	// Start a new OAuth2 Client
	$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);
	$client->setAccessTokenType(1);

	// Set our token
	$client->setAccessToken($token);

	// Set the headers for the request
	$header = array(
		'Authorization' => $token,
		'Content-Type' => 'application/json',
		'Accept' => 'application/json'
		);

	$params = array(
	  'person' => array(
			'email' => $formchangeemail,
			'first_name' => $formchangefirstname,
			'last_name' => $formchangelastname
		)
	);

	$response = $client->fetch(REQUEST_ENDPOINT . '/people/push?access_token='.$token, json_encode($params), 'PUT',$header);

	echo json_encode($response);
else :
	$error = "ID ou token incorrect";
	echo json_encode($error);
endif;
