<html lang="fr-FR">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="UTF-8">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  <meta http-equiv="content-language" content="fr-FR">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

  <title>API NationBuilder Tests</title>
	<meta name="description" content="NationBuilder API developer tests">

	<meta name="robots" content="noindex, nofollow">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.min.css">
</head>
<?php

echo '<h1>API NationBuilder Tests</h1>';
echo '<hr/>';

define('ROOT', getcwd());

// OAuth 2 Library
require_once ROOT . '/OAuth2/Client.php';
require_once ROOT . '/OAuth2/GrantType/IGrantType.php';
require_once ROOT . '/OAuth2/GrantType/AuthorizationCode.php';

// Client ID and Secret from Nation Builder
const CLIENT_ID     = 'd651257f5dfc894fafd554ddf0bfc73d65aed8daad37d8e044be10d782587f5f';
const CLIENT_SECRET = '167f25249aacc26a5b9dfa5389bf60a9018168dbec35c9efaabb36fbf55db63e';

// Constants we need to talk to Nation Builder
const WEBSITE_URI 			      = "https://nbapi.reputationsquad.com/";
const REDIRECT_URI 			      = "https://nbapi.reputationsquad.com/index.php";
const AUTHORIZATION_ENDPOINT	= "https://reputationsquaddev.nationbuilder.com/oauth/authorize";
const TOKEN_ENDPOINT		      = "https://reputationsquaddev.nationbuilder.com/oauth/token";
const REQUEST_ENDPOINT		    = "https://reputationsquaddev.nationbuilder.com/api/v1";

// Start a new OAuth2 Client
$client = new OAuth2\Client(CLIENT_ID, CLIENT_SECRET);

// echo $_GET['code'];

$code = '';
$token = '';

if(!empty($_POST)) :
	// echo 'POST : <pre>'; var_dump($_POST); echo '</pre>';
	$page = $_SERVER['PHP_SELF'];
	$sec = "0.25";
	header("Refresh: $sec; url=$page");
endif;

if ( isset($_POST['apicode']) ) $code = $_POST['apicode']; else $code = '';
if ( isset($_POST['apitoken']) ) $token = $_POST['apitoken']; else $token = '';

if ( isset($_POST['formaddfirstname']) ) $formaddfirstname = $_POST['formaddfirstname']; else $formaddfirstname = '';
if ( isset($_POST['formaddlastname']) ) $formaddlastname = $_POST['formaddlastname']; else $formaddlastname = '';
if ( isset($_POST['formaddemail']) ) $formaddemail = $_POST['formaddemail']; else $formaddemail = '';


if ( isset($_POST['formchangefirstname']) ) $formchangefirstname = $_POST['formchangefirstname']; else $formchangefirstname = '';
if ( isset($_POST['formchangelastname']) ) $formchangelastname = $_POST['formchangelastname']; else $formchangelastname = '';
if ( isset($_POST['formchangeemail']) ) $formchangeemail = $_POST['formchangeemail']; else $formchangeemail = '';


// echo '<br /><br />token = '.$token.'<br />';

if ( $token == '') {
	if ( $code == '') {
		// Check if we have an authorize code, if not obtain one

		// echo $_GET['code'];

		if ( !isset($_GET['code']) && $_GET['code']=='' ) {

			echo 'On relance le token / code';
			// Generate the URL we need to go to get the token
			$auth_url = $client->getAuthenticationUrl(AUTHORIZATION_ENDPOINT, REDIRECT_URI);

			// Redirect to that URL (which then sends us back to the REDIRECT_URI with a code, IE this page)
			header('Location: ' . $auth_url);

			// End this process
			die('Redirect');
		}

		$code = $_GET['code'];

	}


	// We have a code, so generate the parameters we need to get an access token
	$params = array('code' => $code, 'redirect_uri' => REDIRECT_URI);

	// Obtain our access token
	$response = $client->getAccessToken(TOKEN_ENDPOINT, 'authorization_code', $params);

	// echo '<pre>';
	// var_dump($response);
	// echo '</pre>';

	// See if we got a valid token back or an error
	if (isset($response['result']['error'])) {
		switch($response['result']['error']) {
			case 'invalid_grant':
				// We can just redirect back to the URI to get a new code in this case.
				//NOT RECOMMENDED FOR PRODUCTION USE.
				header('Location: ' . REDIRECT_URI);

	 			$error = "<b>ERROR</b>: Invalid Grant. This code is invalid, expired, or revoked.<br>";
				break;

			default:
				$error = "<b>Unknown error:</b> " . $response['result']['error'] . " - " .
					$response['result']['error_description'] . "<br>";
				break;
		}

		// End execution and display error message.
		die($error);
	}

	// Parse out the token from the response
	$token = $response['result']['access_token'];
}


// Set our token type (ACCESS_TOKEN_BEARER)
$client->setAccessTokenType(1);

// Set our token
$client->setAccessToken($token);

// Set the headers for the request
$header = array(
	'Authorization' => $token,
	'Content-Type' => 'application/json',
	'Accept' => 'application/json'
	);


// $baseApiUrl = 'https://foobar.nationbuilder.com';
// $client->setAccessToken($token);
$response = $client->fetch(REQUEST_ENDPOINT .'/people');

echo '<h2>People :</h2>';
// echo '<pre>';
// var_dump($response);
// echo '</pre>';

echo '<div class="row">';
foreach( $response["result"]["results"] as $r ):
	echo '<div class="column">';
		echo '<img src="'.$r["profile_image_url_ssl"].'" alt="'.$r["first_name"].'" />  ';
		echo $r["first_name"]. ' '.$r["last_name"].'  ';
		echo '<br/><button type="button" class="button button-outline peoplechange">Change</button><br/>';
		echo '<form class="formpeoplechange" name="formpeoplechange" style="display:none;" action="" method="post">
			<input type="text" class="formchangefirstname" name="formchangefirstname" placeholder="First name" value="'.$r["first_name"].'" required />
			<input type="text" class="formchangelastname" name="formchangelastname" placeholder="Last name" value="'.$r["last_name"].'" required />
			<input type="hidden" class="formchangeid" name="formchangeid" value="'.$r["id"].'" required />
			<input type="text" class="formchangeemail" name="formchangeemail" placeholder="Email" value="'.$r["email"].'" required />
			<input type="hidden" class="apicode" name="apicode" value="'.$code.'" />
			<input type="hidden" class="apitoken" name="apitoken" value="'.$token.'" />
			<button type="button" class="button formpeoplechangebutton">Ok</button>
			<button type="button" class="button button-outline formpeopledelete">Delete</button>
		</form>';
	echo '</div>';
	echo '<br />';
endforeach;
echo '</div>';

?>
<br/><br/>
<div>
<button type="button" id="peopleadd" class="button button-outline">Add people</button>
<form id="formpeopleadd" name="formpeopleadd" style="display:none;" action="" method="post">
	<input type="text" class="formaddfirstname" name="formaddfirstname" placeholder="First name" required />
	<input type="text" class="formaddlastname" name="formaddlastname" placeholder="Last name" required />
	<input type="text" class="formaddemail" name="formaddemail" placeholder="Email" required />
	<input type="hidden" class="apicode" name="apicode" value="<?php echo $code;?>" />
	<input type="hidden" class="apitoken" name="apitoken" value="<?php echo $token;?>" />
	<button type="submit" class="formpeopleaddbutton button" />Add</button>
</form>
</div>
<br />


<script>

</script>
<?php

echo '<hr/>';

echo '<h2>Events :</h2>';

$response = $client->fetch(REQUEST_ENDPOINT .'/sites/reputationsquaddev/pages/events');

foreach( $response["result"]["results"] as $r ):
	// echo '<pre>';
	// var_dump($r);
	// echo '</pre>';

  echo '<div style="border:1px dotted #ccc;padding: 10px; margin: 10px;">';

    echo '<h3>'.$r["name"].'</h3>';
    echo '<p>'.$r["intro"].'</p>';
    // echo ' - '.$r["id"];

    $startDate = date("d-m-Y", strtotime($r["start_time"]));
    $startHour   = date("H:i", strtotime($r["start_time"]));

    echo '<p>Date : '.$startDate.' à '.$startHour.'</p>';

  	echo '<p><a href="https://'.$r["site_slug"].'.nationbuilder.com'.$r["path"].'" target="_blank">-> Lien vers l\'event</a></p>';

  	echo '<button type="button" class="button button-outline eventchange">Change this event</button>';

  	echo '<form name="formeventchange" class="formeventchange" style="display:none;" action="" method="post">
  		<input type="text" class="eventchangetitle" name="eventchangetitle" placeholder="Title" value="'.$r["name"].'" required/><br>
  		<textarea class="eventchangeintro" name="eventchangeintro" placeholder="Intro">'.$r["intro"].'</textarea><br/>
  		Date start : <input type="date" class="eventchangestartdate" name="eventchangestartdate" placeholder="Start Date" value="'.date("Y-m-d", strtotime($r["start_time"])).'" required /><input type="time" class="eventchangestarthour" name="eventchangestarthour" placeholder="Start Hour" value="'.$startHour.'" /><br/>
  		Date end : <input type="date" class="eventchangeenddate" name="eventchangeenddate" placeholder="End Date" value="'.date("Y-m-d", strtotime($r["end_time"])).'" /><input type="time" class="eventchangeendhour" name="eventchangeendhour" placeholder="End Hour" value="'.date("H:i", strtotime($r["end_time"])).'"/><br />
  		<input type="hidden" class="eventid" name="eventid" value="'.$r["id"].'" />
      <input type="hidden" class="eventapicode" name="eventapicode" value="'.$code.'" />
  		<input type="hidden" class="eventapitoken" name="eventapitoken" value="'.$token.'" />
  		<button type="button" class="button formeventchangebutton">Ok</button>
      <button type="button" class="button button-outline formeventdeletebutton">Delete</button>
  	</form>';

  echo '</div>';

endforeach;

?>

<br/><br/>
<button type="button" id="eventadd" class="button button-outline">Add an event</button>
<form id="formeventadd" name="formeventadd" style="display:none;" action="" method="post">
	<input type="text" class="eventaddtitle" name="eventaddtitle" placeholder="Title" required /><br>
	<textarea class="eventaddintro" name="eventaddintro" placeholder="Intro"></textarea><br/>
	Date start : <input type="date" class="eventaddstartdate" name="eventaddstartdate" placeholder="Start Date" required /><input type="time" class="eventaddstarthour" name="eventaddstarthour" placeholder="Start Hour" /><br/>
	Date end : <input type="date" class="eventaddenddate" name="eventaddenddate" placeholder="End Date" /><input type="time" class="eventaddendhour" name="eventaddendhour" placeholder="End Hour" /><br />
	<input type="hidden" class="eventapicode" name="eventapicode" value="<?php echo $code;?>" />
	<input type="hidden" class="eventapitoken" name="eventapitoken" value="<?php echo $token;?>" />
	<button type="submit" class="button formeventaddbutton">Add this event</button>
</form>

<script>


</script>
<script src="assets/js/scripts.js"></script>
